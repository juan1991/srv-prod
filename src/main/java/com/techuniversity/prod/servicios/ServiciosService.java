package com.techuniversity.prod.servicios;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ServiciosService {

    MongoCollection<Document> servicios;

    private static MongoCollection<Document> getServiciosCollection(){

        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();

        MongoClient cliente = MongoClients.create(settings);
        MongoDatabase database = cliente.getDatabase("backMayo");

        return database.getCollection("servicios");

    }

    public static List getAll(){

        MongoCollection<Document> servicios = getServiciosCollection();
        List list = new ArrayList();
        FindIterable<Document> iterador = servicios.find();
        Iterator it = iterador.iterator();

        while(it.hasNext()){
            list.add(it.next());
        }

        return list;

    }

    public static void insert(String cadenaServicio) throws Exception {
        Document doc = Document.parse(cadenaServicio);
        MongoCollection<Document> servicios = getServiciosCollection();
        servicios.insertOne(doc);
    }

    public static void insertBatch(String cadenaServicios) throws Exception {
        Document doc = Document.parse(cadenaServicios);
        List<Document> lstServicios = doc.getList("servicios", Document.class);
        MongoCollection<Document> servicios = getServiciosCollection();
        servicios.insertMany(lstServicios);
    }

    public static List<Document> getFiltrados(String cadenaFiltro){
        MongoCollection<Document> servicios = getServiciosCollection();
        List lista = new ArrayList();
        Document docFiltro = Document.parse(cadenaFiltro);

        FindIterable<Document> iterador = servicios.find(docFiltro);
        Iterator it = iterador.iterator();

        while(it.hasNext()){
            lista.add(it.next());
        }

        return lista;

    }

    public static List<Document> getFiltradosPeriodo(Document docFiltro){
        MongoCollection<Document> servicios = getServiciosCollection();
        List lista = new ArrayList();

        FindIterable<Document> iterador = servicios.find(docFiltro);
        Iterator it = iterador.iterator();

        while(it.hasNext()){
            lista.add(it.next());
        }

        return lista;

    }

    public static void update(String filtro, String valores){
        MongoCollection<Document> servicios = getServiciosCollection();
        Document docFiltro = Document.parse(filtro);
        Document docValores = Document.parse(valores);
        servicios.updateOne(docFiltro, docValores);
    }
}
