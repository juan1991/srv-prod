package com.techuniversity.prod.controllers;

import com.techuniversity.prod.productos.ProductoModel;
import com.techuniversity.prod.productos.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/productos")
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    public ProductoService getProductoService() {
        return productoService;
    }

    public void setProductoService(ProductoService productoService) {
        this.productoService = productoService;
    }

    @GetMapping("/productos")
    public List<ProductoModel> getProductos(@RequestParam(defaultValue = "-1") String page){

        int iPage = Integer.parseInt(page);
        iPage -= 1;

        if (iPage == -1){
            return this.getProductoService().findAll();
        } else{
            return this.getProductoService().findPaginado(iPage);
        }

    }

    @GetMapping("productos/{id}")
    public Optional<ProductoModel> getProductoId(@PathVariable String id){
        return this.getProductoService().findById(id);
    }

    @PostMapping("/productos")
    public ProductoModel insertProducto(@RequestBody ProductoModel nuevo){
        this.getProductoService().save(nuevo);
        return nuevo;
    }

    @PutMapping("/productos")
    public void updateProducto(@RequestBody ProductoModel nuevo){
        this.getProductoService().save(nuevo);
    }

    @DeleteMapping("/productos")
    public boolean deleteProducto(@RequestBody ProductoModel producto){
        return this.getProductoService().deleteProducto(producto);
    }

}