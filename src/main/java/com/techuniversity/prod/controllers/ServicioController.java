package com.techuniversity.prod.controllers;

import com.mongodb.util.JSON;
import com.techuniversity.prod.servicios.ServiciosService;
import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/servicios")
public class ServicioController {

    /*
    @GetMapping("servicios")
    public List getServicios(){
        return ServiciosService.getAll();
    }
    */
    @PostMapping("/servicios")
    public String setServicios(@RequestBody String cadena){
        try{
            Document doc = Document.parse(cadena);
            List<Document> lstServicios = doc.getList("servicios", Document.class);

            if (lstServicios != null){
                ServiciosService.insertBatch(cadena);
            }else{
                ServiciosService.insert(cadena);
            }
            return "OK";
        }catch(Exception e){
            return e.getMessage();
        }
    }

    @GetMapping("/servicios")
    public List getServicios(@RequestBody String filtro){
        return ServiciosService.getFiltrados(filtro);
    }

    @GetMapping("/servicios/periodo")
    public List getServiciosPeriodo(@RequestParam String periodo){
        Document doc = new Document();
        doc.append("disponibilidad.periodos", periodo);
        return ServiciosService.getFiltradosPeriodo(doc);
    }

    @PutMapping("/servicios")
    public String updServicios(@RequestBody String data){
        try{
            JSONObject json = new JSONObject(data);
            String filtro = json.getJSONObject("filtro").toString();
            String valores = json.getJSONObject("valores").toString();
            ServiciosService.update(filtro, valores);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "OK";
    }

}